import React, { Component } from 'react';
import home_slide_1 from './images/home-slide-1.png';
import mob_home_img from './images/mob-home-img.png';
import appstore from './images/appstore.png';
import googleplay from './images/googleplay.png';
import watchvideoIcon from './images/watchvideoIcon.png';

class TopBanner extends Component {
   handleClick = (e) => {
    document.getElementById('videopopup').classList.add('show');
	document.getElementById('bannervideo').setAttribute('src', "https://www.youtube.com/embed/-u4h24Vz940");
	
  }
  videoWrapClick = (e) => {
    document.getElementById('videopopup').classList.remove('show');
	document.getElementById('bannervideo').setAttribute('src', "");
  }
  render() {
    return (
	<div className="container-fluid">
	  <div className="row">
		<div id="homeslider" className="carousel slide" data-ride="carousel" data-interval="5000">
		  <div className="carousel-inner">
			<div className="carousel-item active desktop"> 
				<img className="slideimg desktop" src={home_slide_1} alt="..." />
			  <div className="carousel-caption d-none d-md-block slidecaption">
				<h2>MILITARY FAMILIES DESERVE AMERICA'S MOST POWERFUL FINANCIAL MEMBERSHIP<sup>SM</sup></h2>
				<p>In honor of Veterans Day, MoneyLion is donating $5 to Semper Fi fund for every new member.</p>
				<div className="onlinestore"> <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer"><img src={appstore} alt="appstore" /></a> <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer"><img src={googleplay} alt="googleplay" /></a> </div>
				<span className="watchvideo" data-toggle="modal" onClick={this.handleClick}><img src={watchvideoIcon} alt="playIcon" />Watch Video</span> </div>
			</div>
			<div className="mob-caption slidecaption mob-home-wrap">
				<h2>MILITARY FAMILIES<br />DESERVE<br />AMERICA'S<br />MOST POWERFUL<br />FINANCIAL<br />MEMBERSHIP<sup>SM</sup></h2>
				<div className="mob-home-desc">
					
					<span className="watchvideo" data-toggle="modal" onClick={this.handleClick}><span><img src={watchvideoIcon} alt="playIcon" />Watch Video</span></span><span>In honor of Veterans Day, MoneyLion is donating $5 to Semper Fi fund for every new member.</span>
				</div>				                
				<div className="mob-home-img">
					<img src={mob_home_img} className="mob-home-image"  alt=""/>
				<div className="onlinestore"> <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer"><img src={appstore} alt="appstore"  /></a> 
				<a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank"  rel="noopener noreferrer"><img src={googleplay} alt="googleplay"  /></a> </div>
				</div>
			</div>
			<div id="videopopup" className="modal " onClick={this.videoWrapClick}>
				<div className="modal-dialog">
					<div className="modal-content">
						<div className="modal-body">
							<iframe title="frame-2" id="bannervideo" width="560" height="315" src="" frameBorder="0" allowFullScreen></iframe>
						</div>
					</div>
				</div>
			</div>
		  </div>
		</div>
		  
	  </div>
	</div>
    );
  }
}


export default TopBanner;

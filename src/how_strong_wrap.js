import React, { Component } from 'react';


class HowStrongWrap extends Component {
  render() {
    return (
	<div className="section-moneylion">
		<div className="container text-center">
			<h2 className="mb-5 title-text">How strong is MoneyLion Nation?</h2>
			<div className="row">
				<div className="col-md-4">
					<h2>3 MILLION</h2>
					<div className="description-0-334">Americans</div>
				</div>
				<div className="col-md-4">
					<h2>2 MILLION</h2>
					<div className="description-0-334">financial accounts</div>
				</div>
				<div className="col-md-4">
					<h2>$89 BILLION</h2>
					<div className="description-0-334" >in transactions</div>
				</div>
			</div>
		</div>
	</div>
    );
  }
}


export default HowStrongWrap;

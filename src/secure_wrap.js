import React, { Component } from 'react';
import shield from './images/shield.png';
import logo from './images/logo.png';

class SecureWrap extends Component {
  render() {
    return (
	<div className="section-money-secure text-center mb-5">	
			 <div className="container">
			 	<h1 className="title-text my-5">Your money is secure</h1>
			 	<div className="row">
			 		<div className="col-md-6">
			 			<div className="money-icons">
			 				<img className="dlr-icon  mb-3" src={shield} alt=""/>
			 			</div>
			 			<div><p>Your banking funds are held by Lincoln Savings Bank, a member of FDIC, and deposits are insured up to $250,000.</p></div>
			 		</div>
			 			<div className="col-md-6">
			 				<div className="money-icons">
			 					<img className="spice-icon" src={logo} alt=""/>
			 				</div>
			 				<p className="p-4">Your investment funds are held by our broker-dealer and qualified custodian Drivewealth LLC, and your cash and investments are protected by SIPC up to $500,000, with a limit of $250,000 for cash.</p>
			 			</div>
			 	</div>
			 </div>
		</div>
    );
  }
}


export default SecureWrap;

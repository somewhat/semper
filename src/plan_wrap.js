import React, { Component } from 'react';


class PlanWrap extends Component {
  render() {
    return (
	<div className="secttion-costs">
		<div className="container">
			<div className="wrap text-center">
				<h2 className="my-5 title-text">Choose the plan for you</h2>
			</div>
	
			<div className="row">
				<div className="col-md-9">
					<div className="row">
						<div className="col-md-6 p-5 bg-cost">
							<div className="membership">
								<h3>Core membership</h3>											
								<div><sup>$</sup>0<sub>/mo</sub></div>											
							</div>
							<div className="membership-desc">
								<strong>Total freedom, zero cost:</strong>
								<p>Everything you need to save, earn, and build, for free. You've never seen anything like this</p>
							</div>
	
							<ul className="check-mark">
								<li>Zero-fee checking account<sup className="small">SM</sup></li>
								<li>Zero-fee managed investing<sup className="small">SM</sup></li>
								<li>0% APR Instacash<sup className="small">SM</sup> cash advances</li>
								<li>Up to 12% cashback rewards<sup className="small">SM</sup></li>
								<li>Free credit monitoring</li>
								<li>$25 gift cards rewards program</li>
								<li>55,000 fee-free ATMs</li>
							</ul>
							<div className="diamond">$526 of annual value</div>
							<div className="heart">MoneyLion will donate $5 to Semper Fi Fund for each new member.</div>
							<a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" className="joinbutton white"  rel="noopener noreferrer">join for $0/month</a>
						  </div>
							<div className="col-md-6 p-5">
								<div className="membership">
									<h3>Plus membership</h3>											
									<div><sup>$</sup>29<sub>/mo</sub></div>											
								</div>
								<div className="membership-desc">
									<strong>Dollar A Day On Us:</strong>
									<p>You get $1/day in your investment account just for logging in. Do the math: You get your entire membership fee back if you log in every day!</p>
								</div>		
	
								<ul className="check-mark">
									<li>Zero-fee checking account<sup className="small">SM</sup></li>
									<li>Zero-fee managed investing<sup className="small">SM</sup></li>
									<li>0% APR Instacash<sup className="small">SM</sup> cash advances</li>
									<li>Up to 12% cashback rewards<sup className="small">SM</sup></li>
									<li>Free credit monitoring</li>
									<li>$25 gift cards rewards program</li>
									<li>55,000 fee-free ATMs</li>
									<li>$500 5.99% APR Credit Builder Loans<sup className="small">SM</sup></li>
									<li>Customized managed investment portfolios</li>
									<li>$1 daily cashback for logging into the app</li>
									<li>Free weekly credit score updates</li>
									<li>Access to members only <br />Plus Facebook community</li>
								</ul>
								<div className="diamond">Over $1,000 of annual value</div>
								<div className="heart">MoneyLion will donate $5 to Semper Fi Fund for each new member.</div>
								<a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" className="joinbutton" rel="noopener noreferrer">join for $29/month</a>
							</div>
									  
						  </div>
						</div>
				   </div>
			</div>
		</div>
    );
  }
}


export default PlanWrap;
